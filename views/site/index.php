<?php
/* @var $this yii\web\View */
use yii\widgets\ActiveForm;
use yii\helpers\Html;

$this->title = 'Web Development Company - WEBITIS';
$this->registerMetaTag([
    'name' => 'description',
    'content' => 'WEBITIS has been a software partner for a number of tech companies in ticketing, media and financial areas for 5+ years, developing customized web solutions using PHP, HTML, CSS, Yii Framework technologies, we provide effective web-based solution for business and individual start-up projects.',
]);
$this->registerMetaTag([
    'name' => 'keywords',
    'content' => 'Web sites, Web applications, Cloud solutions, Responsive design, Mobile sites, CRM, PHP, HTML, CSS, YII, CMS',
]);
?>

<!--BANNER START-->
<div id="banner" class="section-padding">
    <div class="container">
        <div class="row">
            <div class="jumbotron">
                <h1 class="small">Welcome To <span class="bold">WebItIs</span></h1>
                <a href="#service" class="btn btn-banner">Learn More<i class="fa fa-send"></i></a>
            </div>
        </div>
    </div>
</div>
<!--BANNER END-->

<!--CTA1 START-->
<div class="cta-1">
    <div class="container">
        <div class="row text-center white">
            <h2 class="cta-title" style="text-transform: uppercase;">Web Development Company</span></h2>
        </div>
    </div>
</div>
<!--CTA1 END-->

<!--SERVICE START-->
<div id="service" class="section-padding">
    <div class="container">
        <div class="row">
            <div class="page-title text-center">
                <h2>Our Service</h2>
                <hr class="pg-titl-bdr-btm">
            </div>
            <div class="col-md-4">
                <div class="service-box">
                    <div class="service-icon"><i class="fa fa-picture-o"></i></div>
                    <div class="service-text">
                        <h3>Frontend Development</h3>
                        <ul>
                            <li>LANDING PAGE DESIGN</li>
                            <li>RESPONSIVE WEB DESIGN</li>
                            <li>CUSTOM WEB DESIGN</li>
                            <li>WEBSITE RE-DESIGN</li>
                        </ul>
                    </div>
                </div>
            </div>
            <div class="col-md-4">
                <div class="service-box">
                    <div class="service-icon"><i class="fa fa fa-code"></i></div>
                    <div class="service-text">
                        <h3>Backend Development</h3>
                        <ul>
                            <li>E-COMMERCE SOLUTIONS</li>
                            <li>CLOUD SOFTWARE SOLUTIONS</li>
                            <li>HIGH-LOAD DEVELOPMENT</li>
                            <li>CUSTOM WEB APPLICATIONS</li>
                        </ul>
                    </div>
                </div>
            </div>
            <div class="col-md-4">
                <div class="service-box">
                    <div class="service-icon"><i class="fa fa-diamond"></i></div>
                    <div class="service-text">
                        <h3>SEO & Maintenance</h3>
                        <ul>
                            <li>SEARCH ENGINE OPTIMIZATION</li>
                            <li>WEBSITE MAINTENANCE</li>
                            <li>WEBSITE SUPPORT</li>
                            <li>SEO AUDIT</li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!--SERVICE END-->

<!--CTA2 START-->
<div class="cta2">
    <div class="container">
        <div class="row white text-center">
            <h3 class="wd100 fnt-22">“Walking on water and developing software from a specification are easy if both are
                frozen.” - Edward V Berard</h3>
        </div>
    </div>
</div>
<!--CTA2 END-->

<!--PORTFOLIO START-->
<div id="portfolio" class="section-padding">
    <div class="container">
        <div class="row">
            <div class="page-title text-center">
                <h2>Our Previous Works</h2>
                <hr class="pg-titl-bdr-btm">
            </div>
            <div class="port-sec">
                <div class="col-md-12 fil-btn text-center">
                    <div class="filter wrk-title active" data-filter="all">Show All</div>
                    <div class="filter wrk-title" data-filter=".category-1">Frontend</div>
                    <div class="filter wrk-title" data-filter=".category-2">Backend</div>
                    <div class="filter wrk-title lst-cld" data-filter=".category-3">SEO</div>
                </div>
                <div id="Container">
                    <div class="filimg mix category-1 category-2 category-3 col-md-4 col-sm-4 col-xs-12" data-myorder="2">
                        <img src="/img/work1.png" class="img-responsive">
                    </div>
                    <div class="filimg mix category-2 category-3 col-md-4 col-sm-4 col-xs-12" data-myorder="4">
                        <img src="/img/work2.png" class="img-responsive">
                    </div>
                    <div class="filimg mix category-1 category-2 col-md-4 col-sm-4 col-xs-12" data-myorder="1">
                        <img src="/img/work3.png" class="img-responsive"></div>
                    <div class="filimg mix category-2 col-md-4 col-sm-4 col-xs-12" data-myorder="8">
                        <img src="/img/work4.png" class="img-responsive"></div>
                    <div class="filimg mix category-2 category-3 col-md-4 col-sm-4 col-xs-12" data-myorder="8">
                        <img src="/img/work6.png" class="img-responsive">
                    </div>
                    <div class="filimg mix category-2 col-md-4 col-sm-4 col-xs-12" data-myorder="8">
                        <img src="/img/work5.png" class="img-responsive">
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!--PORTFOLIO END-->

<!--CTA2 START-->
<div class="cta2">
    <div class="container">
        <div class="row white text-center">
            <h3 class="wd100 fnt-22">“They don't make bugs like Bunny anymore.” - Olav Mjelde</h3>
        </div>
    </div>
</div>
<!--CTA2 END-->

<!--TEAM START-->
<div id="about" class="section-padding">
    <div class="container">
        <div class="row">
            <div class="page-title text-center">
                <h2>Meet Our Team</h2>
                <hr class="pg-titl-bdr-btm">
            </div>
            <div class="autoplay">
                <div class="col-md-6">
                    <div class="team-info">
                        <div class="img-sec">
                            <img src="/img/agent_i.jpg" class="img-responsive">
                        </div>
                        <div class="fig-caption">
                            <h3>Ivan Vovchak</h3>
                            <p class="marb-20">Sr. Web Developer</p>
                            <p>Follow me:</p>
                            <ul class="team-social">
                                <li class="bgblue-dark">
                                    <a href="https://www.facebook.com/ivan.vovchak" target="_blank"><i class="fa fa-facebook"></i></a>
                                </li>
                                <li class="bgred">
                                    <a href="https://plus.google.com/114326790498652736448" target="_blank"><i class="fa fa-google-plus"></i></a>
                                </li>
                                <li class="bgblue-dark">
                                    <a href="https://www.linkedin.com/in/ivan-vovchak-546bbba0/" target="_blank"><i class="fa fa-linkedin"></i></a>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="team-info">
                        <div class="img-sec">
                            <img src="/img/agent_b.jpg" class="img-responsive">
                        </div>
                        <div class="fig-caption">
                            <h3>Bohdan Vovchak</h3>
                            <p class="marb-20">Jr. Web Developer</p>
                            <p>Follow me:</p>
                            <ul class="team-social">
                                <li class="blue-dark">
                                    <a href="https://www.facebook.com/bohdan.vovchak" target="_blank"><i class="fa fa-facebook"></i></a>
                                </li>
                                <li class="red">
                                    <a href="https://plus.google.com/111293478961255421627" target="_blank"><i class="fa fa-google-plus"></i></a>
                                </li>
                                <li class="blue-dark">
                                    <a href="https://www.linkedin.com/in/bohdan-vovchak-8b919bb1/" target="_blank"><i class="fa fa-linkedin"></i></a>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!--TEAM END-->

<!--CTA2 START-->
<div class="cta2">
    <div class="container">
        <div class="row white text-center">
            <h3 class="wd100 fnt-22">“Talk is cheap. Show me the code.” - Linus Torvalds</h3>
        </div>
    </div>
</div>
<!--CTA2 END-->

<!--CONTACT START-->
<div id="contact" class="section-padding">
    <div class="container">
        <div class="row">
            <div class="page-title text-center">
                <h2>Quick Contact</h2>
                <hr class="pg-titl-bdr-btm">
            </div>
            <?php if (Yii::$app->session->hasFlash('contactFormSubmitted')): ?>
                <div class="alert alert-success">
                    Thank you for contacting us. We will respond to you as soon as possible.
                </div>
            <?php endif; ?>
            <div class="form-sec">
                <?php $form = ActiveForm::begin([
                    'id' => 'contact-form',
                    'class' => 'contactForm',
                    'enableAjaxValidation' => true,
                ]); ?>
                <?= $form->field($model, 'name', ['options' => ['class' => 'col-md-4 form-group']])
                    ->textInput([
                        'placeholder' => 'Your Name',
                        'class' => 'form-control text-field-box',
                    ])
                    ->label(false) ?>
                <?= $form->field($model, 'email', ['options' => ['class' => 'col-md-4 form-group']])
                    ->textInput([
                        'placeholder' => 'Your Email',
                        'class' => 'form-control text-field-box',
                    ])
                    ->label(false) ?>
                <?= $form->field($model, 'subject', ['options' => ['class' => 'col-md-4 form-group']])
                    ->textInput([
                        'placeholder' => 'Subject',
                        'class' => 'form-control text-field-box',
                    ])
                    ->label(false) ?>
                <?= $form->field($model, 'body', ['options' => ['class' => 'col-md-12 form-group']])
                    ->textarea([
                        'rows' => 5,
                        'placeholder' => 'Message',
                        'class' => 'form-control text-field-box',
                    ])
                    ->label(false) ?>
                <div class="form-group">
                    <?= Html::submitButton('Submit Now', ['class' => 'button-medium', 'id' => 'contact-submit', 'name' => 'contact']) ?>
                </div>
                <?php ActiveForm::end(); ?>
            </div>
        </div>
    </div>
</div>
<!--CONTACT END-->
