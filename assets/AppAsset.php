<?php

namespace app\assets;

use yii\web\AssetBundle;

class AppAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $css = [
        'css/bootstrap.min.css',
        'css/font-awesome.min.css',
        '//fonts.googleapis.com/css?family=Open+Sans:300,600|Raleway:600,300|Josefin+Slab:400,700,600italic,600,400italic',
        'css/slick-team-slider.css',
        'css/style.css',
    ];
    public $js = [
        'js/jquery.easing.min.js',
        'js/bootstrap.min.js',
        'js/jquery.mixitup.js',
        'js/slick.min.js',
        'js/custom.js',
    ];
    public $depends = [
        'yii\web\YiiAsset',
    ];
}
